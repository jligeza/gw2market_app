from django.conf.urls import patterns, include, url
from django.contrib import admin
from app.views import archive

urlpatterns = patterns(
    '',
    url(r'^$', include('app.urls')),
    url(r'^app/', include('app.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/profile/', archive),
    # TODO zmienic to pozniej na normanla strone profilowa uzytkownika
)
