# -*- coding: utf-8 -*-
import secrets
"""
Django settings for projekt project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secrets key used in production secrets!
SECRET_KEY = secrets.SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = secrets.DEBUG

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1']

# Application definition

# SSLIFY_DISABLE_FOR_REQUEST = [
#     secrets.SSLIFY_acc,
#     secrets.SSLIFY_app,
#     secrets.SSLIFY_admin,
# ]

INSTALLED_APPS = (
    'django.core.mail',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app',
    'registration',
    'chartkick',
    # secrets.SSLSERVER,
)

SITE_ID = 1

# dni dane na aktywację maila i autologowanie
# po kliknięciu linka aktywacyjnego
ACCOUNT_ACTIVATION_DAYS = 5
REGISTRATION_AUTO_LOGIN = True

EMAIL_HOST = secrets.EMAIL_HOST
EMAIL_PORT = secrets.EMAIL_PORT
EMAIL_HOST_USER = secrets.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = secrets.EMAIL_HOST_PASSWORD
EMAIL_USE_SSL = secrets.EMAIL_USE_SSL

"""
    Gmail SMTP port (TLS): 587
        Gmail SMTP port (SSL): 465
    Gmail SMTP TLS/SSL required: yes
"""

MIDDLEWARE_CLASSES = (
    # 'sslify.middleware.SSLifyMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': './debug.log',
            'formatter': 'custom',
        },
    },
    'formatters': {
        'custom': {
            'format': '[%(asctime)s] <%(levelname)s> module:%(module)s' +
            ': %(message)s',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

ROOT_URLCONF = 'projekt.urls'

WSGI_APPLICATION = 'projekt.wsgi.application'

DATABASES = secrets.DATABASES
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pl'

TIME_ZONE = 'Europe/Warsaw'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

import chartkick
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    chartkick.js(),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)
