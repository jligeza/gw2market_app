from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from render.posts import render_posts
from render.bookmarks import render_bookmarks
from render.item_page import render_item_page
from render.database_tools import render_database_tools
from render.item_table import render_item_table


def item_page(request, item_id):
    return render_item_page(request, item_id)


@login_required
def bookmarks(request, page):
    return render_bookmarks(request, page)


@staff_member_required
def database_tools(request):
    return render_database_tools(request)


def archive(request):
    return render_posts(request)


def item_table(request):
    return render_item_table(request)
