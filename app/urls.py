from django.conf.urls import patterns, url
import app.views as views
import app.api as api

urlpatterns = patterns(
    '',
    url(r'^$', views.item_table),
    url(r'^api/$', api.api),
    url(r'^news/$', views.archive, name="archive"),
    url(r'^item_table/$', views.item_table, name="item_table"),
    url(r'^database_tools/$', views.database_tools, name="database_tools"),
    url(r'^bookmarks/([A-Za-z0-9_]{1,15})/$',
        views.bookmarks, name="bookmarks"),
    url(r'^item/(\d+)/$', views.item_page, name="item_page"),
)
