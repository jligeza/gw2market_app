# -*- coding: utf-8 -*-
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from jsonfield import JSONField


class Post(models.Model):
    title = models.CharField(max_length=150)
    body = models.TextField()
    timestamp = models.DateTimeField()

    def __unicode__(self):
        return self.title

admin.site.register(Post)


class Item(models.Model):
    item_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    level = models.IntegerField()
    icon = models.CharField(max_length=150)
    rarity = models.ForeignKey('Item_rarity')
    general_type = models.ForeignKey('Item_type')
    detailed_type = models.ForeignKey('Item_detailed_type')

    def __unicode__(self):
        return self.name

admin.site.register(Item)


class New_market_data(models.Model):
    item_id = models.IntegerField(primary_key=True)
    data = JSONField()

    def __unicode__(self):
        return "ID:" + str(self.item_id)

admin.site.register(New_market_data)


class Bookmarks(models.Model):
    user_id = models.ForeignKey(User)
    items = JSONField()

    def __unicode__(self):
        return self.user_id.get_username()

admin.site.register(Bookmarks)


# 3 dictionaries:
class Item_rarity(models.Model):
    name = models.CharField(max_length=50, primary_key=True)

    def __unicode__(self):
        return self.name

admin.site.register(Item_rarity)


class Item_type(models.Model):
    name = models.CharField(max_length=50, primary_key=True)

    def __unicode__(self):
        return self.name

admin.site.register(Item_type)


class Item_detailed_type(models.Model):
    name = models.CharField(max_length=50, primary_key=True)

    def __unicode__(self):
        return self.name

admin.site.register(Item_detailed_type)
