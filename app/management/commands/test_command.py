from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.test_command()

    def test_command(self):
        f = open('test', 'w')
        f.write("halo")
        f.close()
