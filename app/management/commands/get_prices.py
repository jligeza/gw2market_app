from django.core.management.base import BaseCommand
from ...helpers import prices, items
import app.models as models
import time
from datetime import datetime


class Command(BaseCommand):

    _Item = models.Item
    _audit = None
    _starting_time = 0
    _finishing_time = 0
    _bulk_of_ids = None
    _prices = None

    def handle(self, *args, **options):
        self._price_download_procedure()

    def _price_download_procedure(self):
        counter = 0
        while counter < 2:
            try:
                self._open_download_audit()
                self._time_start()
                self._fetch_items_ids_from_database()
                self._try_download_prices()
                self._check_data_integrity()
                prices.insert_into_database(self._prices)
                self._time_stop()
                self._close_download_audit('[success]')
                return
            except:
                counter += 1
        error = "...price download procedure failed"
        self._audit.write(error)
        self._close_download_audit('[fail]')

    def _open_download_audit(self):
        self._audit = open('PRICES_DOWNLOAD.log', 'a')
        self._audit.write(str(datetime.now())[:-7]+' - downloading prices...')

    def _time_start(self):
        self._starting_time = time.time()

    def _time_stop(self):
        self._finishing_time = time.time()

    def _get_processing_time(self):
        return str(int(self._finishing_time - self._starting_time))

    def _fetch_items_ids_from_database(self):
        self._bulk_of_ids = self._Item.objects.values_list('item_id')
        if len(self._bulk_of_ids) == 0:  # database table empty?
            items.download_and_insert_into_database()
            self._bulk_of_ids = self._Item.objects.values_list('item_id')

    def _close_download_audit(self, message):
        info = '...' + message + '(' + self._get_processing_time() +\
            ' seconds)\n'
        self._audit.write(info)
        self._audit.close()

    def _try_download_prices(self):
        try:
            self._prices = prices.download_all_prices()
        except Exception, e:
            self._audit.write('...error: cannot download prices:\n' + e)
            raise Exception

    def _check_data_integrity(self):
        if len(self._bulk_of_ids) != len(self._prices):
            warning = '\n...downloaded quantity of items does not match' +\
                      ', downloading new item list...'
            self._audit.write(warning)
            try:
                items.download_and_insert_into_database()
                info = "\nnew items downloaded successfully..."
                self._audit.write(info)
            except:
                error = "\ncannot download new item list"
                self._audit.write(error)
                raise Exception
