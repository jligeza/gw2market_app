import app.models as _models


def get_chart_data(item_id):
    query = _models.New_market_data.objects.get(item_id=item_id)
    return _get_prices_from_query(query)


def _get_prices_from_query(query):
    prices_buys = []
    prices_sells = []
    date = []
    for price_id in query.data:
        prices_buys.append(query.data[price_id]['buy_p'])
        prices_sells.append(query.data[price_id]['sell_p'])
        date.append(query.data[price_id]['date'])

    return date, prices_buys, prices_sells
