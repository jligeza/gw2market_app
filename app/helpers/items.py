# coding: utf-8
from django.db import transaction as _transaction
import app.models as _models
import gw2api as _gw2api
import threading as _threading


def search_by_name(name, limit=10):
    match_quantity = _models.Item.objects.filter(name__icontains=name).count()
    items = _models.Item.objects.filter(name__icontains=name)[:limit]
    display_quantity = len(items)
    return _pack_and_return(items, match_quantity, display_quantity)


def download_and_insert_into_database():
    items = _download_all_items()
    _remove_old_items_from_database()
    _insert_new_items_into_database(items)


def get_item_details(item_id):
    try:
        item = _models.Item.objects.get(item_id=item_id)
        data = {'item_id': item.item_id,
                'icon': item.icon,
                'name': item.name,
                'level': item.level,
                'rarity': item.rarity.name,
                'general_type': item.general_type.name,
                }
        try:
            data.update({'detailed_type': item.detailed_type.name})
        except:
            data.update({'detailed_type': ''})
        return data
    except:
        return ''


#
# misc functions
#

def _pack_and_return(items, match_quantity, display_quantity):
    class pack_to_return():
        items = None
        match_quantity = None
        display_quantity = None

        def __init__(self, items, match_quantity, display_quantity):
            self.items = items
            self.match_quantity = match_quantity
            self.display_quantity = display_quantity

    return pack_to_return(items, match_quantity, display_quantity)


def _remove_old_items_from_database():
    old_items = _models.Item.objects.all()
    with _transaction.atomic():
        for item in old_items:
            item.delete()


def _insert_new_items_into_database(items):
    with _transaction.atomic():  # super speed insert
        for item in items:
            _create_item_instance_and_insert_into_database(item)


def _create_item_instance_and_insert_into_database(item):
    detailed_type = _extract_detailed_type(item)
    obj_rarity = _models.Item_rarity(name=item['rarity'])
    obj_item_type = _models.Item_type(name=item['type'])
    obj_item_detailed_type = _models.Item_detailed_type(
        name=detailed_type)

    item_instance = _models.Item(
        item_id=item['id'],
        name=item['name'],
        level=item['level'],
        icon=item['icon'],
        rarity=obj_rarity,
        general_type=obj_item_type,
        detailed_type=obj_item_detailed_type,
        )
    item_instance.save()


def _extract_detailed_type(item):
    try:
        return item['details']['type']
    except:
        return ''


def _download_all_items():
    threads = _create_threads_and_assign_tasks()
    return _collect_items_from_threads(threads)


class _watek(_threading.Thread):

    def __init__(self, query):
        super(_watek, self).__init__()
        self.query = query

    def run(self):
        self.data = _gw2api.get_items_details(self.query)

    def get_items(self):
        return self.data


def _create_threads_and_assign_tasks():
    _MAX_SIZE = 200
    items = _gw2api.get_market_items()
    paczka_index = 0
    paczka = []
    for elem in items:
        paczka.append(elem)
    last_loop = False
    threads = []
    while True:
        query = ""
        for i in xrange(_MAX_SIZE):
            try:
                query = query + str(paczka[paczka_index]) + ","
                paczka_index += 1
            except IndexError:  # index out of range
                last_loop = True
                break
        query = query[:-1]  # usunięcie przecinka z końca łańcucha
        thread = _watek(query)
        thread.start()
        threads.append(thread)
        if last_loop:
            break
    return threads


def _merge_packs(packs_of_items):
    merged_items = []
    for item in packs_of_items:
        for inner_item in item:
            merged_items.append(inner_item)
    return merged_items


def _collect_items_from_threads(threads):
    packs_of_items = []
    for thread in threads:
        thread.join()
        packs_of_items.append(thread.get_items())
    return _merge_packs(packs_of_items)
