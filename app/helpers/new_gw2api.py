#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time as _time
import urllib3 as _urllib3
import json as _json


def get_prices(item_id):
    data = _get_data_from_api(item_id)
    sell = _get_sell_prices_and_dates(data)
    buy = _get_buy_prices_and_dates(data)
    return {'sell': sell, 'buy': buy}


def _get_data_from_api(item_id):
    """Returns all prices for given item id."""
    http = _urllib3.PoolManager()
    url = 'http://www.gw2spidy.com/chart/' + str(item_id)
    request = http.request("GET", url)
    return _json.loads(request.data)


def _get_buy_prices_and_dates(prices):
    raw_buy_price = _select_all_prices('buy', prices)
    # buy_price = _format_price(raw_buy_price)
    raw_buy_date = _select_all_dates('buy', prices)
    buy_date = _format_date(raw_buy_date)
    return {'prices': raw_buy_price, 'dates': buy_date}


def _select_all_prices(buy_or_sell, prices):
    pack = []
    if buy_or_sell == 'buy':
        for price in prices[5]['data']:
            pack.append(price[1])
        return pack
    elif buy_or_sell == 'sell':
        for price in prices[2]['data']:
            pack.append(price[1])
        return pack
    else:
        raise AttributeError('pick buy or sell')


def _select_all_dates(buy_or_sell, prices):
    pack = []
    if buy_or_sell == 'buy':
        for price in prices[5]['data']:
            pack.append(price[0])
        return pack
    elif buy_or_sell == 'sell':
        for price in prices[2]['data']:
            pack.append(price[0])
        return pack
    else:
        raise AttributeError('pick buy or sell')


def _get_sell_prices_and_dates(prices):
    raw_sell_price = _select_all_prices('sell', prices)
    # sell_price = _format_price(raw_sell_price)
    raw_sell_date = _select_all_dates('sell', prices)
    sell_date = _format_date(raw_sell_date)
    return {'prices': raw_sell_price, 'dates': sell_date}


def _format_price(raw_prices):
    """Example return: 125g, 00s, 02c"""
    pack = []
    for raw_price in raw_prices:
        string_price = str(raw_price)
        if '.' in string_price:
            if len(string_price[0:-7]) == 0:
                gold = '0'
            else:
                gold = string_price[0:-7]
            if len(string_price[-7:-5]) == 0:
                silver = '0'
            else:
                silver = string_price[-7:-5]
            copper = string_price[-5:-3]
        else:
            if len(string_price[0:-5]) == 0:
                gold = '0'
            else:
                gold = string_price[0:-5]
            if len(string_price[-5:-3]) == 0:
                silver = '0'
            else:
                silver = string_price[-5:-3]
            copper = string_price[-3:-1]
        pack.append(gold + 'g, ' + silver + 's, ' + copper + 'c')
    return pack


def _format_date(raw_dates):
    pack = []
    for date in raw_dates:
        date /= 1000
        periods = _time.localtime(date)
        formatted = str(periods.tm_year) + '-'
        if periods.tm_mon > 9:
            formatted += str(periods.tm_mon) + '-'
        else:
            formatted += '0' + str(periods.tm_mon) + '-'
        if periods.tm_mday > 9:
            formatted += str(periods.tm_mday) + ' '
        else:
            formatted += '0' + str(periods.tm_mday) + ' '
        if periods.tm_hour > 9:
            formatted += str(periods.tm_hour) + ':'
        else:
            formatted += '0' + str(periods.tm_hour) + ':'
        if periods.tm_min > 9:
            formatted += str(periods.tm_min) + ':'
        else:
            formatted += '0' + str(periods.tm_min) + ':'
        if periods.tm_sec > 9:
            formatted += str(periods.tm_sec) + '.000000 +0200'
        else:
            formatted += '0' + str(periods.tm_sec) + '.000000 +0200'
        pack.append(formatted)
    return pack
