# coding: utf-8
# TODO super brzydkie, jak się nudzi to poprawię
import gw2api as _gw2api
import threading as _threading
import time as _time
from django.db import transaction as _transaction
from datetime import datetime as _datetime
import app.models as _models


def download_all_prices():
    """Zwraca ceny wszystkich przedmiotów"""
    _MAX_SIZE = 200
    ids = _models.Item.objects.values_list('item_id')
    if len(ids) < 1:
        raise ValueError("*** no items in database ***")

    items = []
    for element in ids:
        items.append(element[0])

    paczka_index = 0
    paczka = []
    for elem in items:
        paczka.append(elem)

    last_loop = False
    threads = []

    count = 0
    while True:
        query = ""
        for i in xrange(_MAX_SIZE):
            try:
                query = query + str(paczka[paczka_index]) + ","
                paczka_index += 1
            except IndexError:  # index out of range
                last_loop = True
                break

        query = query[:-1]  # usunięcie przecinka z końca łańcucha

        thread = _watek_prices(query)
        _try_starting_thread(thread)
        count += 1
        threads.append(thread)

        if last_loop:
            break

    items_bulk = []
    for thread in threads:
        thread.join()
        items_bulk.append(thread.get_items())

    # wątki zwracają paczki po 200 itemów - należy
    # te paczki połączyć w jedną
    items_to_return = []
    for element in items_bulk:
        for inner_el in element:
            items_to_return.append(inner_el)

    return items_to_return


def insert_into_database(prices):
    date = str(_datetime.now())
    with _transaction.atomic():  # super speed
        for price in prices:
            New_entry = None
            download_id = 0
            try:
                New_entry = _models.New_market_data.\
                    objects.get(item_id=price['id'])
                download_id = _get_download_id(New_entry)
            except:
                New_entry = _models.New_market_data.\
                    objects.create(item_id=price['id'], data={})
            New_entry.item_id = price['id']
            json = {'date': date,
                    'buy_p': price['buys']['unit_price'],
                    'buy_q': price['buys']['quantity'],
                    'sell_q': price['sells']['quantity'],
                    'sell_p': price['sells']['unit_price']}
            New_entry.data[download_id] = json
            New_entry.save()


def _get_download_id(New_entry):
    max_num = max([int(i) for i in New_entry.data.keys()])
    return max_num + 1


class _watek_prices(_threading.Thread):

    def __init__(self, query):
        super(_watek_prices, self).__init__()
        self.query = query

    def run(self):
        self.data = _gw2api.get_prices(self.query)

    def get_items(self):
        return self.data


def _try_starting_thread(thread):
    while True:
        try:
            thread.start()
            break
        except:
            _time.sleep(1)
