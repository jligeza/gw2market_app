def format_coins(copper):
    gold, rest = _convert_to_gold(copper)
    silver, rest = _convert_to_silver(rest)
    return _coins_to_string(gold, silver, rest)


def _convert_to_gold(copper):
    gold = copper / 10000
    return gold, copper - gold * 10000


def _convert_to_silver(copper):
    silver = copper / 100
    return silver, copper - silver * 100


def _coins_to_string(gold, silver, copper):
    coins = ''
    if gold > 0:
        coins = str(gold) + ' gold'
    if silver > 0:
        if gold > 0:
            coins += ', '
        coins += str(silver) + ' silver'
    if copper > 0:
        if silver > 0:
            coins += ', '
        elif gold > 0:
            coins += ', '
        coins += str(copper) + ' copper'
    return coins
