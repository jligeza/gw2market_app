import app.models as _model


def insert_items(request, items_ids, page):
    bookmarks = _get_or_create_bookmarks_model(request)
    _update_bookmarks(request, bookmarks, items_ids, page)


def delete_item(request, page):
    id_to_delete = request.GET.get('delete_item')
    json = _model.Bookmarks.objects.get(user_id=request.user.id)
    try:
        json.items[page].remove(int(id_to_delete))
        json.save()
    except:
        pass


def get_items(request, page, MAX=300):
    json = None
    items = []
    try:
        json = _model.Bookmarks.objects.get(user_id=request.user.id)
        items_ids = json.items[page]
        items = _model.Item.objects.filter(item_id__in=items_ids)[:MAX]
    except Exception, e:
        print 'EXCEPTION:', e
    return items


def check_if_item_exists(request, item_id, page):
    bookmarks = _get_or_create_bookmarks_model(request)
    if int(item_id) in bookmarks.items[page]:
        return True
    else:
        return False


def get_all_pages(request):
    bookmarks = _get_or_create_bookmarks_model(request)
    pages = []
    for page in bookmarks.items.keys():
        if page == bookmarks.items['selected_page']:
            pages.append({'name': page, 'selected': True})
        else:
            pages.append({'name': page, 'selected': False})
        if pages[-1]['name'] == 'selected_page':
            pages.pop()
    return sorted(pages)


def select_page(request, page):
    bookmarks = _get_or_create_bookmarks_model(request)
    return _try_selecting_bookmarks_page(bookmarks, page)


def create_page(request, page):
    bookmarks = _get_or_create_bookmarks_model(request)
    if _try_creating_new_bookmarks_page(bookmarks, page):
        return True
    else:
        return False


def delete_page(request, page):
    if page == 'default':
        return
    bookmarks = _get_or_create_bookmarks_model(request)
    return _try_deleting_bookmarks_page(bookmarks, page)


def get_selected_page(request):
    bookmarks = _get_or_create_bookmarks_model(request)
    return bookmarks.items['selected_page']


# ***********
# *********** misc functions
# ***********


def _get_ids_of_items(items_to_insert):
    ''' depreciated '''
    ids_to_add = []
    for item in items_to_insert:
        items = _model.Item.objects.values('item_id').\
            filter(name=item)
        for item_id in items:
            ids_to_add.append(item_id['item_id'])
    return ids_to_add


def _get_or_create_bookmarks_model(request):
    try:
        return _model.Bookmarks.objects.get(user_id=request.user.id)
    except:
        return _create_bookmarks_structure_in_db(request)


def _create_bookmarks_structure_in_db(request):
    bookmarks_query =\
        _model.Bookmarks.objects.filter(user_id=request.user.id)
    bookmarks_query.delete()
    json = {'selected_page': 'default', 'default': []}
    return _model.Bookmarks.objects.create(items=json,
                                           user_id_id=request.user.id)


def _update_bookmarks(request, json, items_ids, page):
    if isinstance(items_ids, list):
        for item_id in items_ids:
            if not int(item_id) in json.items[page]:
                json.items[page].append(int(item_id))
    else:
        if not int(items_ids) in json.items[page]:
            json.items[page].append(int(items_ids))
    _update_database(request, json.items)


def _update_database(request, current_ids):
    # TODO optimize, no need to delete and rewrite everythin anew
    bookmarks_query =\
        _model.Bookmarks.objects.filter(user_id=request.user.id)
    bookmarks_query.delete()
    _model.Bookmarks.objects.create(items=current_ids,
                                    user_id_id=request.user.id)


def _page_exists(bookmarks, page):
    if page in bookmarks.items.keys():
        return True
    else:
        return False


def _try_selecting_bookmarks_page(bookmarks, page):
    if _page_exists(bookmarks, page):
        bookmarks.items['selected_page'] = page
        bookmarks.save()
        return True
    return False


def _try_creating_new_bookmarks_page(bookmarks, page):
    if _page_exists(bookmarks, page):
        return False
    else:
        bookmarks.items[page] = []
        bookmarks.save()
        return True


def _try_deleting_bookmarks_page(bookmarks, page):
    if _page_exists(bookmarks, page):
        bookmarks.items.pop(page)
        bookmarks.save()
        return page
    else:
        return 'already exists'
