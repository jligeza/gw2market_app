# -*- coding: utf-8 -*-
import urllib3

try:
    import json
except ImportError:
    import simplejson as json

_http = urllib3.PoolManager()


def get_number_of_commerce_items():
    """Get number of items that can be traded on BLTC"""
    url = "https://api.guildwars2.com/v2/commerce/listings/"
    request = _http.request("GET", url)
    return _extract_header(request)


def get_number_of_items():
    """Get number of all items in game"""
    url = "https://api.guildwars2.com/v2/items/"
    request = _http.request("GET", url)
    return _extract_header(request)


def get_items_details(IDs):
    """Get detailed info of specified items.
    Input: IDs
    Output: (items, total_items, downloaded_items)"""
    return _request_v2("items?ids=", IDs)


def get_prices(IDs):
    """Get current prices of specified items.

    Example return:

    {
      "id": 19684,
      "buys": {
        "quantity": 145975,
        "unit_price": 7018
      },
      "sells": {
        "quantity": 126,
        "unit_price": 7019
      }
    }
    """
    return _request_v2("commerce/prices?ids=", IDs)


def get_listings(IDs):
    """Get listings of specified items.

    Example return:

    {
      "id": 19684,
      "buys": [
        { "listings":  23, "unit_price":   1, "quantity": 1962 },
        { "listings":   1, "unit_price":   5, "quantity":   45 },
        { "listings": 105, "unit_price":   9, "quantity": 7033 },
        …
      ],
      "sells": [
        { "listings":   1, "unit_price":  99, "quantity":   211 },
        { "listings":   5, "unit_price": 100, "quantity":   699 },
        { "listings":  74, "unit_price": 101, "quantity": 17746 },
        …
      ]
    }
    """
    return _request_v2("commerce/listings?ids=", IDs)


def get_items():
    """ Get a list of all item IDs.
    Warning: conflicts with commerce (not all IDs are on market).
    """
    return _request_v2("/items/", "")


def get_market_items():
    """Get all items that are available on BLTC."""
    return _request_v2("/commerce/prices/", "")


def _request_v2(json_location, IDs):
    """Send request"""
    url = "https://api.guildwars2.com/v2/" + json_location + str(IDs)
    request = _http.request("GET", url)
    items = json.loads(request.data)
    return _remove_invalid_ids(items, json_location)


def _remove_invalid_ids(items, json_location):
    """Remove bugged IDs"""
    if(json_location == "/commerce/prices/"):
        for i in xrange(len(items) - 1):
            if(items[i] == 29978):
                del items[i]
    return items


def _extract_header(request):
    """Extract max number of items from header"""
    total_items = 0
    try:
        total_items = int(request.getheader("x-result-total"))
    except:
        pass
    return total_items
