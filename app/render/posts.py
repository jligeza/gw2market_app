from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import app.models as models
from django.shortcuts import render


def render_posts(request):
    posts = models.Post.objects.all().order_by("-timestamp")
    paginator = Paginator(posts, 5)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, "archive.html",
                  {'posts': posts, 'user': request.user})
