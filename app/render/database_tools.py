# coding: utf-8
import app.models as _models
from time import time as _time
from django.shortcuts import render as _render
from ..helpers import items as _items
from ..helpers import prices as _prices

_request = None


def render_database_tools(request):
    global _request
    _request = request

    if(request.GET.get('get_items')):
        return _get_items()
    elif(request.GET.get('get_items_prices')):
        return _get_items_prices()
    else:
        return _render_tools_page()


def _get_items():
    start = _time()
    try:
        _items.download_and_insert_into_database()
    except Exception, e:
        return _render_error_page('cannot download items: ' + e)
    execution_time = _time() - start
    return _render_success_page(execution_time,
                                'all items downloaded')


def _get_items_prices():
    start = _time()

    bulk_of_ids = _models.Item.objects.values_list('item_id')
    try:
        prices_ = _prices.download_all_prices()
    except Exception, e:
        return _render_error_page('cannot download prices:' + e)

    answer = _check_number_of_downloaded_items(bulk_of_ids, prices_)
    if answer != 'correct':
        return _render_error_page(answer)

    try:
        _prices.insert_into_database(prices_)
    except Exception as e:
        return _render_error_page(e)

    execution_time = _time() - start
    return _render_success_page(execution_time,
                                'downloaded all prices')


def _check_number_of_downloaded_items(bulk_of_ids, prices_):
    if len(bulk_of_ids) > len(prices_):
        return 'Downloaded too few items'
    elif len(bulk_of_ids) < len(prices_):
        return 'Downloaded too many items'
    return 'correct'


def _render_error_page(answer):
    return _render(_request, 'database_tools_failure.html',
                   {'operation': answer})


def _render_success_page(execution_time, message):
    return _render(_request, 'database_tools_success.html',
                   {'user': _request.user,
                    'time': execution_time,
                    'operation': message})


def _render_tools_page():
    return _render(_request, "database_tools.html", {'user': _request.user})
