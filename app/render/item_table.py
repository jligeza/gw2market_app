from django.shortcuts import render as _render
import app.models as _models
import random as _random
from ..helpers import items as _items


def render_item_table(request):
    if request.GET.get('search_items'):
        return _render_searched_items(request)
    else:
        return _render_random_items(request)


def _render_searched_items(request):
    found_items = _items.search_by_name(request.GET.get('search_items'))
    return _render(request, "item_table.html",
                   {'items': found_items.items,
                    'user': request.user,
                    'searching': True,
                    'display_quantity': found_items.display_quantity,
                    'match_quantity': found_items.match_quantity,
                    })


def _render_random_items(request):
    items = _get_random_items()
    return _render(request, "item_table.html",
                   {'items': items,
                    'user': request.user,
                    'searching': False,
                    'display_quantity': None,
                    'match_quantity': None,
                    })


def _get_random_items():
    numbers = []
    for i in xrange(10):
        while True:
            number = _random.randint(1, 61000)
            if _models.Item.objects.filter(item_id=number).exists():
                numbers.append(number)
                break
    return _models.Item.objects.filter(item_id__in=numbers)
