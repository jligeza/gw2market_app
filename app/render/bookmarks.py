from django.shortcuts import render as _render
from ..helpers import bookmarks as _bookmarks
from ..helpers import items as _items
from ..templatetags.my_tags import bookmark_list as _bookmark_list
from django.http import JsonResponse


def render_bookmarks(request, page):
    if request.GET.get('delete_page'):
        page_to_delete = request.GET.get('delete_page')
        result = _bookmarks.delete_page(request, page_to_delete)
        return JsonResponse({'result': result})

    elif request.GET.get('create_bookmark_page'):
        page_name = request.GET.get('create_bookmark_page')
        result = _bookmarks.create_page(request, page_name)
        if result is True:
            result = 'success'
        else:
            result = 'fail'
        return JsonResponse({'result': result})

    elif request.GET.get('get_bookmark_list'):
        bookmark_pages = _bookmarks.get_all_pages(request)
        html = _bookmark_list(bookmark_pages)
        return JsonResponse({'result': html})

    elif request.GET.get('select_page'):
        page_to_select = request.GET.get('select_page')
        if _bookmarks.select_page(request, page_to_select):
            return JsonResponse({'result': 'success'})
        else:
            return JsonResponse({'result': 'fail'})

    elif request.GET.get('get_selected_page'):
        selected_page = _bookmarks.get_selected_page(request)
        return JsonResponse({'result': selected_page})

    if not _bookmarks.select_page(request, page):
        _bookmarks.select_page(request, 'default')

    selected_page = _bookmarks.get_selected_page(request)

    items_to_insert = None
    display_quantity = None
    match_quantity = None

    if request.GET.get('insert_items'):
        items = request.GET.get('insert_items')
        _bookmarks.insert_items(request, items, selected_page)
        return JsonResponse({'result': 'success'})

    elif request.GET.get('search_by_name'):
        found_items = _items.search_by_name(request.GET.get('search_by_name'))
        json_data = _items_to_json(found_items.items,
                                   found_items.match_quantity,
                                   found_items.display_quantity)
        return JsonResponse(json_data)

    elif request.GET.get('delete_item'):
        _bookmarks.delete_item(request, selected_page)
        return JsonResponse({'result': 'success'})

    elif request.GET.get('get_item_details'):
        item_id = request.GET.get('get_item_details')
        details = _items.get_item_details(item_id)
        return JsonResponse({'item': details})

    elif request.GET.get('check_if_item_exists'):
        item_id = request.GET.get('check_if_item_exists')
        json = _bookmarks.check_if_item_exists(request, item_id, selected_page)
        return JsonResponse({'result': json})

    else:
        pass

    items = _bookmarks.get_items(request, selected_page)
    bookmark_pages = _bookmarks.get_all_pages(request)

    return _render(request, 'bookmarks.html',
                   {'user': request.user,
                    'items': items,
                    'quantity': len(items),
                    'items_to_insert': items_to_insert,
                    'match_quantity': match_quantity,
                    'display_quantity': display_quantity,
                    'bookmark_pages': bookmark_pages,
                    })


def _items_to_json(items, match_quantity, display_quantity):
    data = {'items': {
            'names': [],
            'ids': [],
            'levels': [],
            'icons': [],
            'rarities': [],
            'general_types': [],
            'detailed_types': []
            }}
    for item in items:
        data['items']['names'].append(item.name)
        data['items']['ids'].append(item.item_id)
        data['items']['levels'].append(item.level)
        data['items']['icons'].append(item.icon)
        data['items']['rarities'].append(item.rarity.name)
        try:
            data['items']['general_types'].append(item.general_type.name)
        except:
            data['items']['general_types'].append('')
        try:
            data['items']['detailed_types'].append(item.detailed_type.name)
        except:
            data['items']['detailed_types'].append('')
    data['display_quantity'] = display_quantity
    data['match_quantity'] = match_quantity
    return data
