from django.http import Http404 as _Http404
from django.shortcuts import render as _render
import app.models as _models
from ..helpers.new_gw2api import get_prices


def render_item_page(request, item_id):
    item = _models.Item.objects.filter(item_id=item_id)
    _check_if_item_exists(item)

    data = get_prices(item_id)

    sell = {}
    buy = {}
    START = _select_prices_range(data)

    for i in xrange(START, 0):
        sell[data['sell']['dates'][i]] = data['sell']['prices'][i]
        buy[data['buy']['dates'][i]] = data['buy']['prices'][i]

    date_from = data['buy']['dates'][START]
    date_to = data['buy']['dates'][-1]
    date_from = date_from[0:10]
    date_to = date_to[0:10]

    line_data = [{u'data': sell, u'name': 'sells'},
                 {u'data': buy, u'name': 'buys'}]

    return _render(request, "item_page.html",
                   {'item': item,
                    'line_data': line_data,
                    'date_from': date_from,
                    'date_to': date_to,
                    })


def _check_if_item_exists(item):
    if not item:
        raise _Http404("wrong item ID")


def _select_prices_range(data):
    if len(data['sell']['prices']) < 500:
        return (len(data['sell']['prices'])) * -1 + 1
    else:
        return -500
