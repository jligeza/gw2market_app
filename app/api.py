# coding: utf-8
from django.http import JsonResponse
from .templatetags.my_tags import item_list
from .helpers import bookmarks
from .helpers import items


def api(request):
    if request.GET.get('request') == 'html_item_list':
        if request.GET.get('search_pattern'):
            return _searched_items_json_response(request)
        elif request.GET.get('page'):
            return _bookmarked_items_json_response(request)
    return JsonResponse({'result': 'unknown input'})


def _searched_items_json_response(request):
    search_pattern = request.GET.get('search_pattern')
    display_mode = _set_display_mode(request)
    found_items = items.search_by_name(search_pattern)
    html_item_list = item_list(found_items.items, display_mode)
    return JsonResponse({'html_item_list': html_item_list,
                         'match_quantity': found_items.match_quantity,
                         'display_quantity': found_items.display_quantity})


def _set_display_mode(request):
    mode = request.GET.get('mode')
    if mode is None:
        return 'default'
    return mode


def _bookmarked_items_json_response(request):
    page = request.GET.get('page')
    display_mode = _set_display_mode(request)
    items = bookmarks.get_items(request, page)
    html_item_list = item_list(items, display_mode)
    return JsonResponse({'html_item_list': html_item_list})
