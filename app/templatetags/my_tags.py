# coding: utf-8
from django import template
import app.models as models

register = template.Library()  # django thing, required


@register.simple_tag
def selected_page(user_id):
    """
    Get from database current selected bookmark page of given.
    Input: user_id
    Output: string with url to selected page
    """

    query = _get_bookmarks_of_given_user(user_id)
    return _get_selected_page(query)


def _get_bookmarks_of_given_user(user_id):
    return models.Bookmarks.objects.filter(user_id=user_id)


def _get_selected_page(query):
    if len(query) > 0:
        selected_page = query[0].items['selected_page']
        return '/app/bookmarks/' + selected_page + '/'
    return '/app/bookmarks/default/'


@register.simple_tag
def bookmark_list(bookmark_pages):
    """
    Format bookmark pages into clickable buttons.
    Input: bookmark_pages (QueryDict type)
    Output: html buttons with pages
    """

    html = ''
    for page in bookmark_pages:
        html += _create_button(page)
    return html


def _create_button(page):
    html = ''
    html += _create_buttons_left_side(page)
    html += _create_buttons_right_side(page)
    return html


def _create_buttons_left_side(page):
    html = ''
    if page['selected'] is True:
        html += _selected_button_left_side(page)
    else:
        html += _unselected_button_left_side(page)
    return html


def _selected_button_left_side(page):
    html = ''
    html += '<div id="active" class="bookmark_link %s">' % page['name']
    html += page['name']
    return html


def _unselected_button_left_side(page):
    html = ''
    html += '<div class="bookmark_link %s">' % page['name']
    html += '<a href="/app/bookmarks/%s">%s</a>' %\
            (page['name'], page['name'])
    return html


def _create_buttons_right_side(page):
    html = ''
    if page['name'] != "default":
        html += _nondefault_button_right_side(page)
    else:
        html += _default_button_right_side()
    return html


def _nondefault_button_right_side(page):
    html = ''
    html += '<font onclick="delete_bookmark_page( \'%s\', $(this) )"'\
            % page['name']
    html += u'color="red"> ✗</font>'
    html += '</div> '
    return html


def _default_button_right_side():
    return '</div> '


@register.simple_tag
def item_list(items, mode="default"):
    '''
    Format given items into html table.
    Input: items (QueryDict type)[, mode]
    Output: html item list
    ***
    modes: default, suggested_items, bookmarked_items
    '''

    if _check_mode(mode) is False:
        return 'wrong mode'

    html = ''

    if not items:
        return _create_empty_table(mode)

    html += _begin_table(mode)
    html += _create_headers(mode)
    html += _create_rows_with_items(items, mode)
    html += _finish_table()
    html += _create_search_result_discard_button(mode)

    return html


def _check_mode(mode):
    modes = ('default', 'suggested_items', 'bookmarked_items',)
    if not mode in modes:
        return False
    return True


def _create_empty_table(mode):
    html = ''
    if mode == 'bookmarked_items':
        html += '<table class="item_table" id="main_table">'
    elif mode == 'suggested_items':
        html += '<table class="item_table" id="suggested_items_table">'
    else:
        html += '<table class="item_table">'
    html += '</table>'
    return html


def _begin_table(mode):
    html = ''
    if mode == 'bookmarked_items':
        html += '<table class="item_table" id="main_table">'
    elif mode == 'suggested_items':
        html += '<table class="item_table" id="suggested_items_table">'
    else:
        html += '<table class="item_table">'
    return html


def _create_headers(mode):
    html = '''
        <tr>
            <th>Icon</th>
            <th>Name</th>
            <th>Level</th>
            <th>Rarity</th>
            <th>Type</th>
        '''
    if mode == "suggested_items":
        html += '<th>Save?</th>'
    elif mode == "bookmarked_items":
        html += '<th>Delete?</th>'
    html += '</tr>'
    return html


def _create_rows_with_items(items, mode):
    html = ''
    for item in items:
        html += _create_row(item, mode)
    return html


def _create_row(item, mode):
    html = _begin_row()
    html += _create_item_icon(item)
    html += _create_item_name(item)
    html += _create_item_level(item)
    html += _create_item_rarity(item)
    html += _create_item_type(item)
    html += _create_bookmark_buttons(item, mode)
    html += _finish_row()
    return html


def _begin_row():
    return '<tr>'


def _create_item_icon(item, height=50, width=50):
    html = '<td><img src="' + item.icon
    html += '" height="' + str(height) + '" width="' + str(width) + '"/></td>'
    return html


def _create_item_name(item):
    return '<td><a href=' + '/app/item/' + str(item.item_id) + \
        '> ' + item.name + '</a></td>'


def _create_item_level(item):
    return '<td>' + str(item.level) + '</td>'


def _create_item_rarity(item):
    html = '''
        <td style="text-shadow: -1px 0 black, 0 1px black,1px 0 black,
            0 -1px black">
            <b><font color="
        '''

    if item.rarity.name == "Exotic":
        html += '#ffa405'
    elif item.rarity.name == "Masterwork":
        html += '#2CFF5A'
    elif item.rarity.name == "Rare":
        html += '#fcd00b'
    elif item.rarity.name == "Fine":
        html += '#62A4DA'
    elif item.rarity.name == "Ascended":
        html += '#fb3e8d'
    elif item.rarity.name == "Legendary":
        html += '#4C139D'
    else:
        html += '#ffffff'

    html += ' "> ' + item.rarity.name + ' </font></b></td>'
    return html


def _create_item_type(item):
    html = '<td>' + item.general_type_id

    if item.detailed_type_id:
        html += ', ' + item.detailed_type_id + '</td>'
    else:
        html += '</td>'
    return html


def _create_bookmark_buttons(item, mode):
    html = ''
    if mode == 'suggested_items':
        html += _create_bookmark_button(item)
    if mode == 'bookmarked_items':
        html += _create_bookmark_deletion_button(item)
    return html


def _create_bookmark_button(item):
    html = \
        '''
    <td onclick="add_new_item_to_bookmarks(
    ''' + str(item.item_id) + u''',$(this))">
    <font color="green" size="6">✓</font>
    </td>
        '''
    return html


def _create_bookmark_deletion_button(item):
    html = \
        u'''
    <td onclick="delete_item_from_bookmarks(
    ''' + str(item.item_id) + u''',$(this))">
    <font color="red" size="6">✗</font>
    </td>
        '''
    return html


def _finish_row():
    return '</tr>'


def _finish_table():
    return '</table>'


def _create_search_result_discard_button(mode):
    html = ''
    if mode == 'suggested_items':
        html += '<div style="display:inline-block;" onclick="'
        html += "$('#received_items').fadeOut(700);"
        html += '">Discard search results '
        html += u'<font color="red" size="5">✗</font></div><br><br>'
    return html
