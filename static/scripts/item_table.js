$(document).ready(function() {

    function _create_input_field() {
        var search_input = "<span class=bg>Please enter search pattern: </span>" +
            "<input id='item_search_input' size='30'><br><br>";
        document.getElementById('ajax_item_input').innerHTML = search_input;
    }

    _create_input_field();

    // enter button->confirm item search
    $('#item_search_input').keypress(function(event){
        if(event.which == 13) /*enter*/ {
            var search_pattern = $('#item_search_input').val(); 
            search_items(search_pattern);
        }
    })

    function search_items(search_pattern) {
        $.get( "/app/api/", {'request': 'html_item_list', 'search_pattern': search_pattern})
            .fail(function(){
                 alert('server error: can\'t search items!!!');
            })
            .success(function(data){
                _show_found_items_table(data);
                _show_download_info(data);
            });
    }

    function _show_found_items_table(data) {
        $('#item_table_div').fadeOut(0);
        document.getElementById('item_table_div').innerHTML = data['html_item_list'];
        $('#item_table_div').fadeIn('slow');
    }

    function _show_download_info(data) {
        var download_info = "<i>Displayed " + data['display_quantity'] +
            " out of " + data['match_quantity'] + " database entries.</i>"
        if(!data['match_quantity'])
            download_info = "<br>No such items found in database.";
        document.getElementById('ajax_download_info').innerHTML = download_info;
    }
});
