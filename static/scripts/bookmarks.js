$(document).ready(function() {
    // search items
    $('#search_items').click(function() {
        var search_pattern = $('#item_search_input').val(); 
        _search_items(search_pattern);
    });

    // enter button->confirm item search
    $('#item_search_input').keypress(function(event){
        if(event.which == 13) /*enter*/ {
            var search_pattern = $('#item_search_input').val(); 
            _search_items(search_pattern);
        }
    })

    // enter button->create bookmark page
    $('#new_page_name_input').keypress(function(event){
        if(event.which == 13) /*enter*/ {
            var page_name = $('#new_page_name_input').val(); 
            create_new_bookmark_page(page_name);
        }
    })

    // menu for adding new bookmark pages
	$('#show_hidden_menu').click(function() {
        $('#hidden_menu').fadeIn('slow');
	})

});
function create_new_bookmark_page(page_name) {
    if(_validate_new_page_name(page_name)) {
        _save_page_in_database_and_redraw_list(page_name);
        _fadeOut_bookmark_add_menu();
    }
}

function delete_bookmark_page(page, button_handle) {
    if(confirm('Delete ,,' + page + '" page?')) {
        $('.' + page).fadeOut('slow');
        button_handle.fadeOut('slow');
        $.get( "/app/bookmarks/" + page, {'delete_page': page})
        .success(function(){
            _select_default_page_if_deleting_active(page);
        });
    }
}

function delete_item_from_bookmarks(item_id, td_handle) {
    var page = _get_page_name_from_url();
    td_handle.closest('tr').attr('bgcolor', 'red');
    _delete_item(page, item_id)
    td_handle.closest('tr').fadeOut('slow', function(){ td_handle.closest('tr').remove() });
    if(_bookmarked_items_table_empty()) {
        _show_noItemsRecorded_text();
        _hide_bookmarked_items_table();
    }
}

function add_new_item_to_bookmarks(item_id, td_handle) {
    var page = _get_page_name_from_url();
    td_handle.closest('tr').attr('bgcolor', 'green');
    _add_item(page, item_id);
    td_handle.closest('tr').fadeOut('slow', function(){ td_handle.closest('tr').remove() });
    if(_suggested_items_table_empty()) {
        _hide_suggested_items_div();
    }
}

/*
 *
 *  misc functions
 *
 */

function _save_page_in_database_and_redraw_list(page_name) {
    $.get( "/app/bookmarks/default", {'create_bookmark_page': page_name})
        .fail(function() {
            alert('server error: can\'t create new page');
        })
        .success(function() {
            _redraw_bookmark_list();
        });
}

function _redraw_bookmark_list() {
    $.get( "/app/bookmarks/default", {'get_bookmark_list': 'x'})
        .fail(function() {
            alert('server error: can\'t download bookmark list');
        })
        .success(function(html_list) {
            document.getElementById('bookmark_list').innerHTML = html_list['result'];
        });
}

function _select_default_page_if_deleting_active(page) {
    $.get( "/app/bookmarks/default", {'get_selected_page': 'x'})
        .success(function(selected_page) {
            if(selected_page['result'] == page)
                _select_page_and_redraw_bookmarks_and_items('default');
        });

}
function _select_page_and_redraw_bookmarks_and_items(page) {
    $.get( "/app/bookmarks/default", {'select_page': page})
        .success(function() {
            _redraw_bookmark_list();
            _refresh_bookmarked_items_table(page);
        });
}

function _suggested_items_table_empty() {
    var number_of_rows = $('#suggested_items_table >tbody >tr').length;
    if(number_of_rows <= 2)
        return true;
    else
        return false;
}

function _hide_suggested_items_div() {
    $('#received_items').fadeOut('slow');
}

function _bookmarked_items_table_empty() {
    var number_of_rows = $('#main_table >tbody >tr').length;
    if(number_of_rows <= 2)
        return true;
    else
        return false;

}

function _hide_bookmarked_items_table() {
    var number_of_rows = $('#main_table >tbody >tr').length;
    if(number_of_rows <= 2)
        $('#main_table').fadeOut('slow');
}

function _add_item(page, item_id) {
    $.get( "/app/bookmarks/" + page,
        {'check_if_item_exists': item_id})
        .fail(function(){
             alert('server error: can\'t connect to server');
        })
        .success(function(data){
            if(!_item_already_bookmarked(data))
                _insert_item_into_bookmarks_db_table(page, item_id);
        });
}

function _insert_item_into_bookmarks_db_table(page_name, item_id) {
    $.get( "/app/bookmarks/" + page_name,
        {'insert_items': item_id})
        .fail(function(){
             alert('server error: can\'t bookmark the item');
        })
        .success(function(data){
            _refresh_bookmarked_items_table(page_name);
        });
}

function _refresh_bookmarked_items_table(page) {
    $.get( "/app/api/",
        {'request': 'html_item_list', 'page': page, 'mode': 'bookmarked_items'})
        .fail(function(){
             alert('server error: can\'t download item list');
        })
        .success(function(data){
            $('#main_table').fadeOut(0);
            document.getElementById('main_table').innerHTML = data['html_item_list'];
            $('#main_table').fadeIn('slow');
            _hide_noItemsRecorded_text();
        });
}

function _hide_noItemsRecorded_text() {
    var number_of_rows = $('#main_table >tbody >tr').length;
    if(number_of_rows > 1)
        $('#no_items_recorded_text').fadeOut(0);
}

function _show_noItemsRecorded_text() {
    $('#no_items_recorded_text').fadeIn('slow');
}

function _item_already_bookmarked(data) {
    if(data['result'] == false)
        return false;
    else
        return true;
}

function _validate_new_page_name(page_name) {
    var regex = new RegExp('^[A-Za-z0-9_]{1,15}$');
    if(regex.test(page_name))
        return true;
    else {
        _fadeIn_regex_tip();
        return false;
    }
}

function _fadeOut_bookmark_add_menu() {
    $('#hidden_regex_tip').fadeOut('slow');
    $('#hidden_menu').fadeOut('slow');
} 

function _fadeIn_regex_tip() {
    $('#hidden_regex_tip').fadeIn('slow');
} 

function _get_page_name_from_url() {
    var url = $(location).attr('pathname');
    url = url.split('/');
    url.pop(); // last element is space
    return url.pop();
} 

function _delete_item(page_name, item_id) {
    $.get( "/app/bookmarks/" + page_name,
        {'delete_item': item_id})
        .fail(function(){
             alert('server error: can\'t delete the item');
        })
        .success(function(){
        });
} 

function _item_search_database_query(page_name, search_pattern) {
    $.get( "/app/api/",
        {'request': 'html_item_list', 'search_pattern': search_pattern, 'mode': 'suggested_items'})
        .fail(function(){
             alert('server error: can\'t search items!!!');
        })
        .success(function(data){
            _show_proposed_items(data['html_item_list']);
            _show_download_info(data['display_quantity'], data['match_quantity'])
        });
}

function _show_proposed_items(html_item_list) {
    $('#received_items').fadeOut(0);
    document.getElementById('received_items').innerHTML = html_item_list;
    $('#received_items').fadeIn('slow');
}

function _show_download_info(display_quantity, match_quantity) {
    info = "<i>"+display_quantity+" from "+match_quantity+" matches selected</i>"
    document.getElementById('ajax_download_info').innerHTML = info;
    $('#ajax_download_info').fadeIn('slow');
}

function _search_items(search_pattern) {
    var page_name = _get_page_name_from_url();
    _item_search_database_query(page_name, search_pattern);
}
